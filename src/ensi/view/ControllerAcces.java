package ensi.view;

import ensi.comm.Game_ws;
import ensi.comm.ServerManager;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.io.IOException;


public class ControllerAcces {

    ServerManager serverManager;
    public TextField IPTF;
    public TextField PORTTF;
    public String IP;
    public int port;

    public void connection(MouseEvent mouseEvent) throws IOException {
        serverManager = new ServerManager();
        IP = recupeIP();
        port = recupePort();
        System.out.println(IP + "     " + port);


        serverManager.startConnection(IP, port);


        Game_ws game= new Game_ws();
        game.run();

    }



    public String recupeIP(){
        String IP ="";
        IP = IPTF.getText();
        return IP;
    }
    public int recupePort(){
        int port;
        port = Integer.parseInt(PORTTF.getText());
        return port;
    }

}
