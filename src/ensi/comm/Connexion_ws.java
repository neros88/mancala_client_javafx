package ensi.comm;

import ensi.MancalaClient;
import ensi.view.ControllerAcces;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;

public class Connexion_ws {
    private Parent root_Node;



    private ControllerAcces controller;

    public Connexion_ws() throws IOException {
        /*creation de node pour la connexion*/

        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/acces.fxml"));
        this.root_Node = loader.load();
        this.controller = loader.getController();
    }

    public void run(){

        MancalaClient.stage.setTitle("accesgame");
        MancalaClient.stage.setScene(new Scene(this.root_Node));
        MancalaClient.stage.show();


    }

}
