package ensi.comm;

import ensi.model.Message;

import java.io.*;

import java.net.Socket;
import java.net.SocketException;

/**
 * Created by faye on 01/06/2017.
 */
public class ServerManager {

    private static ServerHandler server;

    public void startConnection(String ip, int port) {

        try {
            server = new ServerHandler(new Socket(ip, port));
            server.start();
            System.out.println("Connected");
            sendMessage(new Message(Message.Type.REQUEST,"message_server", "Message pour le Serveur"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(Message message) {
        this.server.sendMessage(message);
    }

    public void stopConnection() {
        server.stopConnection();
    }

    public static ServerHandler getServer() {
        return server;
    }

    private static class ServerHandler extends Thread {
        private Socket clientSocket;
        OutputStream os;
        InputStream is;
        ObjectOutputStream oos;
        ObjectInputStream ois;
        Message message;

        public ServerHandler(Socket socket) throws IOException {
            this.clientSocket = socket;
            os=clientSocket.getOutputStream();
            oos=new ObjectOutputStream(os);
            is=clientSocket.getInputStream();
            ois=new ObjectInputStream(is);
        }

        public void run() {
            try {
                while (true) {
                    this.message = (Message) ois.readObject();
                    System.out.println("Message received from Serveur : "+message);
                    ServerManager.manageMessage(this.message);
                }

            } catch (SocketException e) {
                System.out.println("Server disconnected");
                this.stopConnection();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        public Message sendMessage(Message message) {
            Message resp = null;
            try {
                oos.writeObject(message);
                System.out.println("Message send to the server : "+message);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return resp;
        }

        public void stopConnection() {
            try {
                os.close();
                is.close();
                clientSocket.close();
                this.interrupt();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void manageMessage(Message message) {
        if (message.type == Message.Type.REQUEST) {
            ServerManager.server.sendMessage(new Message(Message.Type.RESPONSE, "response_server", "Response pour le serveur"));
        } else {

        }
    }
}
