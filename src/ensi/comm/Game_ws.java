package ensi.comm;

import ensi.MancalaClient;
import ensi.view.Controller;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;

public class Game_ws {
    private Parent root_Node;



    private Controller controller;

    public Game_ws() throws IOException {
        /*creation de node pour la connexion*/

        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Mancala.fxml"));
        this.root_Node = loader.load();
        this.controller = loader.getController();
    }

    public void run(){
        MancalaClient.stage.setTitle("game");
        MancalaClient.stage.setScene(new Scene(this.root_Node));
        MancalaClient.stage.show();
    }
}
