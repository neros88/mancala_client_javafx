package ensi.model;

import java.io.Serializable;

public class Message implements Serializable {

    public Type type;
    public String method;
    public Object content;

    public Message(Type type, String method, Object content) {
        this.type = type;
        this.method = method;
        this.content = content;
    }

    public enum Type {
        REQUEST,
        RESPONSE,
    }

    @Override
    public String toString() {
        return "Message{" +
                "type=" + type +
                ", method='" + method + '\'' +
                ", content=" + content +
                '}';
    }
}
